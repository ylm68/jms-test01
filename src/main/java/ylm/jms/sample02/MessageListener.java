package ylm.jms.sample02;

import org.springframework.stereotype.Component;
import ylm.jms.sample02.dto.PersonneDto;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

/**
 * Created by ylm68 on 11/12/2014.
 */
@Component("test02PersonneMessageListener")
public class MessageListener implements javax.jms.MessageListener {

  public void onMessage(Message message) {

    if (message instanceof ObjectMessage) {
      ObjectMessage objectMessage = (ObjectMessage) message;
      try {
          PersonneDto personne = (PersonneDto) objectMessage.getObject();
          System.out.println("message reçu : [" + personne.getPrenom() + " " + personne.getPrenom() + " né le " + personne.getDateDeNaissance() + "]");
      } catch (JMSException e) {
        e.printStackTrace();
      }
    }
  }

}
