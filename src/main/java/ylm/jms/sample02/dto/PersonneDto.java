package ylm.jms.sample02.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by pleroux on 11/12/2014.
 */
public class PersonneDto implements Serializable {

  String prenom;

  String nom;

  Integer taille;

  Date dateDeNaissance;

  public PersonneDto() {
  }

  public PersonneDto(String prenom, String nom, Integer taille, Date dateDeNaissance) {
    this.prenom = prenom;
    this.nom = nom;
    this.taille = taille;
    this.dateDeNaissance = dateDeNaissance;
  }

  public String getPrenom() {
    return prenom;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public Integer getTaille() {
    return taille;
  }

  public void setTaille(Integer taille) {
    this.taille = taille;
  }

  public Date getDateDeNaissance() {
    return dateDeNaissance;
  }

  public void setDateDeNaissance(Date dateDeNaissance) {
    this.dateDeNaissance = dateDeNaissance;
  }

}
