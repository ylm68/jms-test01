package ylm.jms.sample02;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import ylm.jms.sample02.dto.PersonneDto;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ylm68 on 11/12/2014.
 */
public class MessageSender {

  public static void main(String[] args) throws ParseException {

    final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    final String[] dates = {"21/05/1987", "10/08/2003", "26/04/2000"};
    dateFormat.parse("01/05/1987");
    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    final JmsTemplate jmsTemplate = (JmsTemplate) context.getBean("jmsTemplateTest02Personne");
    System.out.println("Début envois");
    for (int i = 1; i <= 10; i++) {
      final int numero = i;
      jmsTemplate.send(new MessageCreator() {
        public ObjectMessage createMessage(Session session) throws JMSException {
          Date date = null;
          try {
            date = dateFormat.parse(dates[numero%3]);
          } catch (ParseException e) {
            date = new Date();
          }
          PersonneDto personne = new PersonneDto("prenom" + numero, "nom" + numero, 10, date);
          System.out.println("message envoyé sur '" + jmsTemplate.getDefaultDestination().toString() + "': [personne" + numero + "]");
          ObjectMessage message = session.createObjectMessage();
          message.setObject(personne);
          return message;
        }
      });
    }

    System.out.println("Fin envois");
  }

}
