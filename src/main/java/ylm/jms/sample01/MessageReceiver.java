package ylm.jms.sample01;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

/**
 * Created by ylm68 on 11/12/2014.
 */
public class MessageReceiver {

  public static void main(String[] args) throws JMSException {

    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    JmsTemplate jmsTemplate = (JmsTemplate) context.getBean("jmsTemplateTest01String");
    jmsTemplate.send(new MessageCreator() {
      public ObjectMessage createMessage(Session session) throws JMSException {
        ObjectMessage message = session.createObjectMessage();
        message.setObject("message 1");
        return message;
      }
    });

    System.out.println("message envoyé sur " + jmsTemplate.getDefaultDestination().toString());
    Message receivedMessage = jmsTemplate.receive("test01StringQueue");
    ObjectMessage msg = (ObjectMessage) receivedMessage;
    System.out.println("message reçu sur " + jmsTemplate.getDefaultDestination().toString() + " : " + msg.getObject().toString());
  }

}
